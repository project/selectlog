# Selective Watchdog
Tiny module that lets you filter watchdog entries for
given backends, based on type and/or severity.
Typical use would be to have all entries logged to syslog
for perfomance reasons, but a few ones still logged to
database for easier debugging.

## Usage
Enable as usual, and add a $conf['selectlog'] entry to your
settings.php file. It must be an array keyed by backend modules
(modules implementing hook_watchdog), with values either of
log entry types keyed by severity, or severities keyed by types.
You can also use '*' as a catch-all marker.
Eg:
<?php

$conf['selectlog'] = array(
  'dblog' => array(
    'mycustom_module' => '*',
    WATCHDOG_CRITICAL => '*',
    WATCHDOG_ERROR => array(
      'node',
      'views',
    ),
    'comment' => array(
      WATCHDOG_ALERT,
      WATCHDOG_DEBUG,
      WATCHDOG_EMERGENCY,
    ),
  ),
);

?>

This would results on dblog module only logging entries that matches:
- any entry of type 'mycustom_module'
- any entries of type WATCHDOG_CRITICAL
- entries of severity WATCHDOG_ERROR and type 'node' or 'views'
- entries of type 'comment' and severity of WATCHDOG_ALERT, WATCHDOG_DEBUG or 
  WATCHDOG_EMERGENCY

Any other watchdog backend modules, eg, syslog, would continue to log all
entries as usual.

## Notes
- Some modules are implementing hook_watchdog but are not actual 
  backend loggers, and instead use the hook to react (rules, etc).
- Enabling the module without any defined $conf['selectlog'] has no effect.
- Only add entries for modules you want to filter on, leave others apart.
  Typically, do not include your default backend (syslog, mongodb, …).
